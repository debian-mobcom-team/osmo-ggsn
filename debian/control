Source: osmo-ggsn
Maintainer: Debian Mobcom Maintainers <Debian-mobcom-maintainers@lists.alioth.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>,
           Ruben Undheim <ruben.undheim@gmail.com>,
           Nate Doris <nate.doris@gmail.com>
Section: net
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               pkgconf,
               libdpkg-perl,
               libosmocore-dev (>= 1.9.2),
               libmnl-dev,
               help2man
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-mobcom-team/osmo-ggsn
Vcs-Git: https://salsa.debian.org/debian-mobcom-team/osmo-ggsn.git
Homepage: https://projects.osmocom.org/projects/openggsn
Rules-Requires-Root: no

Package: osmo-ggsn
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Breaks: openggsn
Replaces: openggsn
Depends: ${shlibs:Depends},
         ${misc:Depends},
         lsb-base
Description: Osmocom Gateway GPRS Support Node (GGSN)
 OsmoGGSN is a Gateway GPRS Support Node (GGSN). It is used by mobile
 operators as the interface between the Internet and the rest of the
 mobile network infrastructure.

Package: libgtp10
Provides: ${t64:Provides}
Replaces: libgtp6t64
Breaks: libgtp6t64 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: library implementing the GTP protocol between SGSN and GGSN
 OsmoGGSN is a Gateway GPRS Support Node (GGSN). It is used by mobile
 operators as the interface between the Internet and the rest of the
 mobile network infrastructure.
 .
 This library is part of OsmoGGSN and implements the GTP protocol between
 SGSN (Serving GPRS support node) and GGSN.

Package: libgtp-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends},
         libgtp10 (= ${binary:Version})
Description: Development files for libgtp
 OsmoGGSN is a Gateway GPRS Support Node (GGSN). It is used by mobile
 operators as the interface between the Internet and the rest of the
 mobile network infrastructure.
 .
 The library libgtp implements the GTP protocol between SGSN and GGSN
 and this package contains the development files for this library.
